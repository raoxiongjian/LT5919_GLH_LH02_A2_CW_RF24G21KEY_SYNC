#ifndef _GLH_FFT_H_
#define _GLH_FFT_H_
#include "glh_typedef.h"

#define uchar uint8
#define uint uint32

struct compx
{
	float real;
	float imag;
};

void fft(struct compx *xin,uchar data N);

#endif