/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：jt_led_mode.c
* 文件标识：
* 摘 要：
*    灯条模式的具体实现
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年7月5日
*/
#include "glh_led_mode.h"
#include "glh_rgb.h"
#include "glh_sys_tick.h"

#define     STATIC_DYNAMIC_GRAD         1
#define     DYNAMIC_GRAD                1       //动态模式的变化梯度
#define     SPEED_PARAM                 1
#define     ON_OFF_VALUE_CHANGE         1
#define     BN_VALUE_CHANGE             1
#define     ON_OFF_VALUE_MAX            255
#define     iResetCompleteFlag()        s_u8CompleteFlag = 0
#define     iSetCompleteFlag()          s_u8CompleteFlag = 1
#define     iIsComplete()               (s_u8CompleteFlag == 1)
#define     iSetRgb(R, G, B)            GLHRGB_SetColor(PWM_CHANNEL_R, R);GLHRGB_SetColor(PWM_CHANNEL_G, G);GLHRGB_SetColor(PWM_CHANNEL_B, B)
#define     iSetRgbcw(R, G, B, C, W)    GLHRGB_SetColor(PWM_CHANNEL_R, R);GLHRGB_SetColor(PWM_CHANNEL_G, G);GLHRGB_SetColor(PWM_CHANNEL_B, B);GLHRGB_SetColor(PWM_CHANNEL_C, C);GLHRGB_SetColor(PWM_CHANNEL_W, W)
#define     MAX_BN                      255
#define     JIANBIAN_MODE_SPEED         (MAX_SPEED - s_u8Speed + 2)
#define     TIAOBIAN_MODE_SPEED         ((MAX_SPEED - s_u8Speed + 1) * 100)
#define     STATIC_MODE_ACTION_TIME     1

typedef void (* MODE_SHADE_F)(void);     //渲染线程

static MODE_SHADE_F XDATA s_pfnModeShade = NULL;       //彩光模式线程

static uint8 XDATA s_u8CurrColorNum = 0;         //总共设置了几个颜色
uint8 XDATA s_u8ColorIndex = 0;           //当前变化到了第几个颜色
uint8 XDATA s_u8CurrR = 0, s_u8CurrG = 0, s_u8CurrB = 0, s_u8CurrC = 0, s_u8CurrW = 0;  //当前的RGB值
static uint8 XDATA s_u8StaticTargetR = 0, s_u8StaticTargetG = 0, s_u8StaticTargetB = 0, s_u8StaticTargetC = 0, s_u8StaticTargetW = 0;  //当前的RGB值
static uint8 XDATA s_u8BretahProgressValue = 0;
static RGB_S XDATA s_sRgb[COLOR_NUM_MAX] = {0};
uint8 XDATA s_u8Flag = 0, s_u8Flag1 = 0, s_u8Flag2 = 0;
uint8 XDATA s_u8CompleteFlag = 0; //动作是否完成
static uint8 XDATA s_u8Speed = 100;
uint32 XDATA u32LastRgbTime = 0;
static uint8 s_u8Cnt = 0;
static BOOL s_bSwitchStatu = OFF;
static uint8 s_u8OnOffValue = 0;
static uint16 s_u16CurrBn = 0, s_u16TargetBn = 0;

code uint16 u16BnTable[BN_MAX] =         //GAMMA值3.0
{
897 ,
964 ,
1034 ,
1108 ,
1186 ,
1266 ,
1351 ,
1439 ,
1531 ,
1626 ,
1726 ,
1829 ,
1937 ,
2049 ,
2165 ,
2285 ,
2409 ,
2538 ,
2672 ,
2810 ,
2953 ,
3101 ,
3253 ,
3411 ,
3573 ,
3740 ,
3913 ,
4091 ,
4274 ,
4463 ,
4656 ,
4856 ,
5061 ,
5272 ,
5489 ,
5711 ,
5940 ,
6174 ,
6415 ,
6661 ,
6914 ,
7173 ,
7439 ,
7711 ,
7990 ,
8275 ,
8567 ,
8866 ,
9172 ,
9484 ,
9804 ,
10131 ,
10465 ,
10806 ,
11155 ,
11511 ,
11874 ,
12245 ,
12624 ,
13010 ,
13404 ,
13807 ,
14217 ,
14635 ,
15061 ,
15495 ,
15938 ,
16389 ,
16849 ,
17317 ,
17793 ,
18278 ,
18772 ,
19275 ,
19787 ,
20308 ,
20837 ,
21376 ,
21924 ,
22482 ,
23048 ,
23625 ,
24210 ,
24806 ,
25411 ,
26025 ,
26650 ,
27284 ,
27929 ,
28583 ,
29248 ,
29923 ,
30608 ,
31304 ,
32010 ,
32727 ,
33454 ,
34192 ,
34940 ,
35700 ,
};

void iStatic(void);
    
static void iResetParams(void)
{
	s_u8Flag = 0;
	s_u8Flag1 = 0;
	s_u8Flag2 = 0;
	s_u8Cnt = 0;
    s_u8ColorIndex = 0;
	s_u8CurrR = 0;
	s_u8CurrG = 0;
	s_u8CurrB = 0;
	s_u8CurrW = 0;
	s_u8BretahProgressValue = 0;
    iResetCompleteFlag();
}
    
void GLHLM_Init(void)
{
    GLHRGB_Init();
    iResetParams();
}


//处理开关机的淡入淡出
static void iOnOffFadeMainThread(void)
{
    static uint32 s_u32LastOnOffChangeTime = 0;

    if(GulSystickCount - s_u32LastOnOffChangeTime >= 1)
    {
        s_u32LastOnOffChangeTime = GulSystickCount;
        
        if(s_bSwitchStatu == ON)
        {
            if(s_u8OnOffValue >= ON_OFF_VALUE_MAX)
            {
                return;
            }
            
            if(s_u8OnOffValue + ON_OFF_VALUE_CHANGE < ON_OFF_VALUE_MAX)
            {
                s_u8OnOffValue += ON_OFF_VALUE_CHANGE;
            }
            else
            {
                s_u8OnOffValue = ON_OFF_VALUE_MAX;
            }
            GLHRGB_SetOnOffValue(s_u8OnOffValue);
        }
        else
        {
            if(s_u8OnOffValue == 0)
            {
                s_u8CurrR = s_u8StaticTargetR;
                s_u8CurrG = s_u8StaticTargetG;
                s_u8CurrB = s_u8StaticTargetB;
                s_u8CurrC = s_u8StaticTargetC;
                s_u8CurrW = s_u8StaticTargetW;
                s_u16CurrBn = s_u16TargetBn;
                return;
            }
            
            if(s_u8OnOffValue > ON_OFF_VALUE_CHANGE)
            {
                s_u8OnOffValue -= ON_OFF_VALUE_CHANGE;
            }
            else
            {
                s_u8OnOffValue = 0;
                iResetParams();
                s_u16CurrBn = 0;
            }
            GLHRGB_SetOnOffValue(s_u8OnOffValue);        
        }
    }
}

//处理亮度设置时的渐变
static void iBnFadeMainThread(void)
{
    static uint32 s_u32LastBnChangeTime = 0;
    uint16 u16BnValueChange = 0;
    uint8 i = 0;

    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
//    if(s_pfnModeShade != iStatic)    //静态模式才处理开机渐变过程
//    {
//        s_u16CurrBn = s_u16TargetBn;
//        GLHRGB_SetBn(s_u16CurrBn);
//        return;
//    }

    if(GulSystickCount - s_u32LastBnChangeTime >= 4)
    {
        s_u32LastBnChangeTime = GulSystickCount;

        for(i=0; i<100; i++)
        {
            if(s_u16CurrBn <= u16BnTable[i])
            {
                break;
            }
        }
        if(i >= 100)
        {
            i = 99;
        }
        u16BnValueChange = (u16BnTable[i] - u16BnTable[i-1]) / 5;
        
        if(s_u16CurrBn < s_u16TargetBn)
        {
            if(s_u16CurrBn + u16BnValueChange < s_u16TargetBn)
            {
                s_u16CurrBn += u16BnValueChange;
            }
            else
            {
                s_u16CurrBn = s_u16TargetBn;
            }
            GLHRGB_SetBn(s_u16CurrBn);        
        }
        else
        {
            if(s_u16TargetBn + u16BnValueChange < s_u16CurrBn)
            {
                s_u16CurrBn -= u16BnValueChange;
            }
            else
            {
                s_u16CurrBn = s_u16TargetBn;
            }
             GLHRGB_SetBn(s_u16CurrBn);
        }
    }
}

void GLHLM_RenderProc(void)
{
    if (s_pfnModeShade != NULL)
    {
        s_pfnModeShade();
    }
    
    iOnOffFadeMainThread();
    iBnFadeMainThread();
}

BOOL GLHLM_GetModeIsComplete(void)
{
    return iIsComplete();
}

static void iCheckIsComplete(void)
{
    s_u8ColorIndex++;
    if (s_u8ColorIndex >= s_u8CurrColorNum)
    {
        s_u8ColorIndex = 0;
        iSetCompleteFlag();   //一次动作执行完成
    }
    else
    {
        iResetCompleteFlag();
    }
}

void GLHLM_SetColorBuff(uint8 u8Index, uint8 u8R, uint8 u8G, uint8 u8B)
{
    if((u8Index >= COLOR_NUM_MAX))
    {
        return;
    }
    
    s_sRgb[u8Index].u8r = u8R;
    s_sRgb[u8Index].u8g = u8G;
    s_sRgb[u8Index].u8b = u8B;
}

void iStatic(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < STATIC_MODE_ACTION_TIME)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrR > s_u8StaticTargetR)
    {
        if(s_u8CurrR - s_u8StaticTargetR >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrR -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_u8StaticTargetR;
        }
    }
    else
    {
        if(s_u8StaticTargetR - s_u8CurrR >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrR += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_u8StaticTargetR;
        }
    }
    
    if(s_u8CurrG > s_u8StaticTargetG)
    {
        if(s_u8CurrG - s_u8StaticTargetG >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrG -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_u8StaticTargetG;
        }
    }
    else
    {
        if(s_u8StaticTargetG - s_u8CurrG >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrG += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_u8StaticTargetG;
        }
    }
    
    if(s_u8CurrB > s_u8StaticTargetB)
    {
        if(s_u8CurrB - s_u8StaticTargetB >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrB -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_u8StaticTargetB;
        }
    }
    else
    {
        if(s_u8StaticTargetB - s_u8CurrB >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrB += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_u8StaticTargetB;
        }
    }
    
    if(s_u8CurrC > s_u8StaticTargetC)
    {
        if(s_u8CurrC - s_u8StaticTargetC >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrC -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrC = s_u8StaticTargetC;
        }
    }
    else
    {
        if(s_u8StaticTargetC - s_u8CurrC >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrC += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrC = s_u8StaticTargetC;
        }
    }
	
    if(s_u8CurrW > s_u8StaticTargetW)
    {
        if(s_u8CurrW - s_u8StaticTargetW >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrW -= STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_u8StaticTargetW;
        }
    }
    else
    {
        if(s_u8StaticTargetW - s_u8CurrW >= STATIC_DYNAMIC_GRAD)
        {
            s_u8CurrW += STATIC_DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrW = s_u8StaticTargetW;
        }
    }

    iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);	
}

/*
    功能: 设置为静态常亮模式,颜色渐变
    输入: 无
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetStaticColor函数设置显示的颜色。
*/
void GLHLM_StaticMode(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8C, uint8 u8W)
{
    s_u8StaticTargetR = u8R;
    s_u8StaticTargetG = u8G;
    s_u8StaticTargetB = u8B;
    s_u8StaticTargetC = u8C;
    s_u8StaticTargetW = u8W;
    
	s_pfnModeShade = iStatic;
    u32LastRgbTime = GulSystickCount - STATIC_MODE_ACTION_TIME;  //为了使模式能够立即执行
}

/*
    功能: 设置为静态常亮模式,立即生效，不用渐变
    输入: 无
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_StaticModeImmediately(uint8 u8R, uint8 u8G, uint8 u8B, uint8 u8C, uint8 u8W)
{
	iResetParams();
    
    s_u8CurrR = u8R;
    s_u8CurrG = u8G;
    s_u8CurrB = u8B;
    s_u8CurrC = u8C;
    s_u8CurrW = u8W;
    
	iSetRgbcw(s_u8CurrR, s_u8CurrG, s_u8CurrB, s_u8CurrC, s_u8CurrW);
	s_pfnModeShade = NULL;
}

static void iFlash(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < TIAOBIAN_MODE_SPEED)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8Flag)
    {
        s_u8CurrR = 0;
        s_u8CurrG = 0;
        s_u8CurrB = 0;
        iSetRgb(s_u8CurrR, s_u8CurrG, s_u8CurrB);
        iCheckIsComplete();      
    }
    else
    {
        s_u8CurrR = s_sRgb[s_u8ColorIndex].u8r;
        s_u8CurrG = s_sRgb[s_u8ColorIndex].u8g;
        s_u8CurrB = s_sRgb[s_u8ColorIndex].u8b;
        iSetRgb(s_u8CurrR, s_u8CurrG, s_u8CurrB);
    }  
    
    s_u8Flag = !s_u8Flag;
}

/*
    功能: 设置为闪烁模式
    输入: u8ColorNum    设置闪烁的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_FlashMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    s_u8CurrColorNum = u8ColorNum;
 
    s_pfnModeShade = iFlash;
    
    u32LastRgbTime = GulSystickCount - TIAOBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iBth(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < JIANBIAN_MODE_SPEED)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrColorNum == 0)
    {
        return;
    }
	
    if (s_u8Flag)
    {
		if(s_u8BretahProgressValue - DYNAMIC_GRAD > 0)
		{
            s_u8BretahProgressValue -= DYNAMIC_GRAD;
		}
		else
	    {
		    s_u8BretahProgressValue = 0;
		}

        s_u8CurrR = s_sRgb[s_u8ColorIndex].u8r*s_u8BretahProgressValue/255;
        s_u8CurrG = s_sRgb[s_u8ColorIndex].u8g*s_u8BretahProgressValue/255;
        s_u8CurrB = s_sRgb[s_u8ColorIndex].u8b*s_u8BretahProgressValue/255;	
		iSetRgb(s_u8CurrR, s_u8CurrG, s_u8CurrB);

        if (s_u8BretahProgressValue == 0)
        {
            s_u8Flag = 0;

            iCheckIsComplete();
        }
    }
    else
    {
		if(s_u8BretahProgressValue + DYNAMIC_GRAD <= MAX_BN)
		{
			s_u8BretahProgressValue += DYNAMIC_GRAD;	
		}
		else
	    {
			s_u8BretahProgressValue = MAX_BN;
		}

        s_u8CurrR = s_sRgb[s_u8ColorIndex].u8r*s_u8BretahProgressValue/255;
        s_u8CurrG = s_sRgb[s_u8ColorIndex].u8g*s_u8BretahProgressValue/255;
        s_u8CurrB = s_sRgb[s_u8ColorIndex].u8b*s_u8BretahProgressValue/255;		
		iSetRgb(s_u8CurrR, s_u8CurrG, s_u8CurrB);

        if (s_u8BretahProgressValue == MAX_BN)
        {
            s_u8Flag = 1;

        }
    }
}

/*
    功能: 设置为呼吸模式
    输入: u8ColorNum    设置呼吸的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_BreathMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iBth;
    
    u32LastRgbTime = GulSystickCount - JIANBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iFade(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < JIANBIAN_MODE_SPEED)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrColorNum == 0)
    {
        return;
    }

    if(s_u8CurrR > s_sRgb[s_u8ColorIndex].u8r)
    {
        if(s_u8CurrR - s_sRgb[s_u8ColorIndex].u8r >= DYNAMIC_GRAD)
        {
            s_u8CurrR -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_sRgb[s_u8ColorIndex].u8r;
        }
    }
    else
    {
        if(s_sRgb[s_u8ColorIndex].u8r - s_u8CurrR >= DYNAMIC_GRAD)
        {
            s_u8CurrR += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrR = s_sRgb[s_u8ColorIndex].u8r;
        }
    }
    
    if(s_u8CurrG > s_sRgb[s_u8ColorIndex].u8g)
    {
        if(s_u8CurrG - s_sRgb[s_u8ColorIndex].u8g >= DYNAMIC_GRAD)
        {
            s_u8CurrG -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_sRgb[s_u8ColorIndex].u8g;
        }
    }
    else
    {
        if(s_sRgb[s_u8ColorIndex].u8g - s_u8CurrG >= DYNAMIC_GRAD)
        {
            s_u8CurrG += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrG = s_sRgb[s_u8ColorIndex].u8g;
        }
    }
    
    if(s_u8CurrB > s_sRgb[s_u8ColorIndex].u8b)
    {
        if(s_u8CurrB - s_sRgb[s_u8ColorIndex].u8b >= DYNAMIC_GRAD)
        {
            s_u8CurrB -= DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_sRgb[s_u8ColorIndex].u8b;
        }
    }
    else
    {
        if(s_sRgb[s_u8ColorIndex].u8b - s_u8CurrB >= DYNAMIC_GRAD)
        {
            s_u8CurrB += DYNAMIC_GRAD;
        }
        else
        {
            s_u8CurrB = s_sRgb[s_u8ColorIndex].u8b;
        }
    }
	
    iSetRgb(s_u8CurrR, s_u8CurrG, s_u8CurrB);
	
	if((s_u8CurrR == s_sRgb[s_u8ColorIndex].u8r) && (s_u8CurrG == s_sRgb[s_u8ColorIndex].u8g)\
        &&(s_u8CurrB == s_sRgb[s_u8ColorIndex].u8b))
	{
		iCheckIsComplete();
	} 
}

/*
    功能: 设置为渐变模式
    输入: u8ColorNum    设置渐变的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_FadeMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
	s_u8ColorIndex = 0;
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iFade;
    
    u32LastRgbTime = GulSystickCount - JIANBIAN_MODE_SPEED;  //为了使模式能够立即执行
}

static void iJump(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < TIAOBIAN_MODE_SPEED)
    {
        return;
    }
	u32LastRgbTime = GulSystickCount;

    if (s_u8ColorIndex >= s_u8CurrColorNum)
    {
        s_u8ColorIndex = 0;
        iSetCompleteFlag();   //一次动作执行完成
		u32LastRgbTime = GulSystickCount - TIAOBIAN_MODE_SPEED;
		return;
    }
    else
    {
        iResetCompleteFlag();
    }

    s_u8CurrR = s_sRgb[s_u8ColorIndex].u8r;
    s_u8CurrG = s_sRgb[s_u8ColorIndex].u8g;
    s_u8CurrB = s_sRgb[s_u8ColorIndex].u8b;		
    iSetRgb(s_u8CurrR, s_u8CurrG, s_u8CurrB);    	

	s_u8ColorIndex++;
}

/*
    功能: 设置为跳变模式
    输入: u8ColorNum    设置跳变的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/

void GLHLM_JumpMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iJump;
    
    u32LastRgbTime = GulSystickCount - 1000000;  //为了使模式能够立即执行,一般的闪烁间隔可定不会超过1000秒，所以减去1000秒应该没问题
}

static void iFireWorks(void)
{
    if(s_bSwitchStatu == OFF)
    {
        return;
    }
    
    if(GulSystickCount - u32LastRgbTime < JIANBIAN_MODE_SPEED)
    {
        return;
    }
    u32LastRgbTime = GulSystickCount;
    
    if(s_u8CurrColorNum == 0)
    {
        return;
    }
	
    if (s_u8Flag)
    {
		if(s_u8BretahProgressValue + DYNAMIC_GRAD <= MAX_BN)
		{
			s_u8BretahProgressValue += DYNAMIC_GRAD;	
		}
		else
	    {
			s_u8BretahProgressValue = MAX_BN;
		}

        if (s_u8BretahProgressValue == MAX_BN)
        {
            s_u8Flag = 0;
            iCheckIsComplete();
        }
    }
    else
    {
		if(s_u8BretahProgressValue - DYNAMIC_GRAD > 0)
		{
            s_u8BretahProgressValue -= DYNAMIC_GRAD;
		}
		else
	    {
		    s_u8BretahProgressValue = 0;
		}

        s_u8CurrR = s_sRgb[s_u8ColorIndex].u8r*s_u8BretahProgressValue/255;
        s_u8CurrG = s_sRgb[s_u8ColorIndex].u8g*s_u8BretahProgressValue/255;
        s_u8CurrB = s_sRgb[s_u8ColorIndex].u8b*s_u8BretahProgressValue/255;		
		iSetRgb(s_u8CurrR, s_u8CurrG, s_u8CurrB);

        if (s_u8BretahProgressValue == 0)
        {
            s_u8Flag = 1;
        }
    }
}

/*
    功能: 设置为烟花模式
    输入: u8ColorNum    设置烟花模式的颜色个数
    输出: 无
    说明：调用此函数前，需要调用GLHLM_SetColorBuff函数设置显示的颜色。
*/
void GLHLM_FireWorksMode(uint8 u8ColorNum)
{
    if ((u8ColorNum == 0) || (u8ColorNum > COLOR_NUM_MAX))
    {
        return;
    }
    
    iResetParams();
    s_u8BretahProgressValue = MAX_BN;
    
    s_u8CurrColorNum = u8ColorNum;
    
    s_pfnModeShade = iFireWorks;
    
    u32LastRgbTime = GulSystickCount - JIANBIAN_MODE_SPEED;  //为了使模式能够立即执行
}



/*
    功能: 设置速度
    输入: u8Speed    速度参数
    输出: 无
    说明：
*/
void GLHLM_SetSpeed(uint8 u8Speed)
{
	if(u8Speed > MAX_SPEED)
    {
		s_u8Speed = MAX_SPEED;
	}
	else if(u8Speed < MIN_SPEED)
    {
		s_u8Speed = MIN_SPEED;
	}
    else
    {
        s_u8Speed = u8Speed;
    }
}

void GLHLM_SetBn(uint8 u8bn)
{
    if(u8bn > BN_MAX)
    {
        return;
    }
    
    s_u16TargetBn = u16BnTable[u8bn - 1];
}

void GLHLM_SetSwitchStatus(BOOL bSwitch)
{
    s_bSwitchStatu = bSwitch;
}