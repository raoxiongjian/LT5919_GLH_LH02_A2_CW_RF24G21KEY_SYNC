/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_pwm.h
* 文件标识：
* 摘 要： 
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年7月5日
*/
#ifndef __GLH_PWM_H__
#define __GLH_PWM_H__
#include "glh_typedef.h"

#ifdef SUPPORT_PWM

#ifndef GLHPWM_MAX
#define GLHPWM_MAX         1658137500    //pwm最大值    255*255*255
#endif

#ifndef GLHBN_MAX
#define GLHBN_MAX         255          //pwm最大值    255*255*255
#endif

#ifndef GLHBN_MIN
#define GLHBN_MIN         1          //pwm最大值    255*255*255
#endif

#ifndef GLHON_OFF_STATE_VALUE_MAX
#define GLHON_OFF_STATE_VALUE_MAX         255          //pwm最大值    255*255*255
#endif

typedef enum _GLHPWM_CHANNEL_E
{
    E_PWM_CH0 = 0x01,    //PWM0   
    E_PWM_CH1 = 0x02,    //PWM1   
    E_PWM_CH2 = 0x04,    //PWM2   
    E_PWM_CH3 = 0x08,    //PWM3  
    E_PWM_CH4 = 0x10,    //PWM4  
    E_PWM_CH5 = 0x20,    //PWM5 
}GLHPWM_CHANNEL_E;


/*
    功能: 初始化IO控制模块
    输入: un8Ch   需要初始化的通道集合
*/
void GLHPWM_Init(uint8 u8Ch);



/*
    功能: 设置每个输出通道的PWM占空比,用于PWM控制方式
    输入:
        duty       占空比
*/
void GLHPWM_Set(GLHPWM_CHANNEL_E eCh, uint32 u32Duty);

#endif //__GLH_PWM_H__

#endif //ifdef SUPPORT_PWM

