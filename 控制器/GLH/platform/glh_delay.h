/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_delay.h
* 文件标识：
* 摘 要：
*   不同平台的延时
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月25日
*/
#ifndef __GLH_DELAY_H__
#define __GLH_DELAY_H__
#include "glh_typedef.h"

/**
    @功能: ms延时函数
    @参数: 
        ms[in]: 需要延时的毫秒数
    @返回: 
    @备注: 
*/
void GLHDL_DelayMs(uint16 ms);

/**
    @功能: us延时函数
    @参数: 
        us[in]:  需要延时的微妙数
    @返回: 
    @备注: 如果延时超过1ms请用ms延时接口，该接口没有清除看门狗
*/
void GLHDL_DelayUs(uint16 us);

#endif //__JT_DELAY_H__

