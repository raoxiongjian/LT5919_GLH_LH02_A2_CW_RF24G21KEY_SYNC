/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称：jt_infrared.h
* 文件标识：
* 摘 要：
*   处理红外事件
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2020年10月9日
*/
#ifndef __JT_INFRARED_H__
#define __JT_INFRARED_H__
#include "jt_typedef.h"

// 红外GPIO定义
#ifdef SUPPORT_IR
#ifndef JTIR_GPIO
#error "Must define JTIR_GPIO"
#endif
#endif

extern uint32 g_u32IrData;

/*
    @功能: 接收完成的数据回调
    @参数: 
    @返回:  
    @备注: 需要应用层自行实现
*/
extern void JTIR_EventCb(void);

/**
    @功能: 初始化红外模块
    @参数: 
    @返回: 
*/
void JTIR_Init(void);

/**
    @功能: 红外模块工作主线程，需要在主循环中不停调用
    @参数: 
    @返回: 
*/
void JTIR_MainThread(void);

/**
    @功能: 查询红外信号是否有效
    @参数:
    @返回: TRUE有效
*/
BOOL JTIR_GetSignalIsValid(void);

#endif //__JT_INFRARED_H__