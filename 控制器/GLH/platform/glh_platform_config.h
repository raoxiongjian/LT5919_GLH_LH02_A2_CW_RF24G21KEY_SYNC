/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称: jt_platform_config.h
* 文件标识：
* 摘 要：
*   平台层统一配置文件
* 当前版本: V1.0
* 作者: raoxiongjian
* 完成日期: 2020年11月29日
*/
#ifndef __JT_PLATFORM_CONFIG_H__
#define __JT_PLATFORM_CONFIG_H__

#include "Platform.h"

#endif //__JT_PLATFORM_CONFIG_H__
