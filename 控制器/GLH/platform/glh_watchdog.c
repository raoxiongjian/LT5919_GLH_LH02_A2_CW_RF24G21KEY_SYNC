/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_watchdog.c
* 文件标识：
* 摘 要：
*   看门狗
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2022年6月2日
*/
#include "glh_watchdog.h"
#include "MS51_16K.H"


void GLHWD_Init(void)  
{
    EA = 0;
    TA=0xAA;TA=0x55;WDCON=0x07;						//Setting WDT prescale 
	set_WDCLR;										//Clear WDT timer
	while((WDCON|~SET_BIT6)==0xFF);				    //confirm WDT clear is ok before into power down mode
	EA = 1;
	set_WDTR;										//WDT run
}

void GLHWD_Start(void)
{

}

void GLHWD_Stop(void)
{

}

void GLHWD_Clear(void)
{
    set_WDCLR;										//Clear WDT timer
}

