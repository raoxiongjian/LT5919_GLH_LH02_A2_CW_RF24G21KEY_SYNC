/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_key.c
* 文件标识：
* 摘 要：
*   按键处理
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年8月9日
*/
#include "MS51_16K.H"
#include "glh_key.h"
#include "glh_sys_tick.h"

#ifdef SUPPORT_KEY

#define GLHKEY_VALID_LEVEL           0
#define GLHKEY_THREAD_INTERVAL       10          // 10ms检测一次
#define GLHKEY_SHORT_KEY_TICK        3
#define GLHKEY_LONG_KEY_TICK         200         //
#define GLHKEY_DOUBLE_INTERVAL       200         // 200ms内双击两次都算双击
#define GLHKEY_CONTINUOUS_TICK       10         //按键长按不松开时，多长时间触发一次

typedef uint8 (* GET_IO_LEVEL_F)(void);

typedef struct _GLH_KEY_S
{
    GET_IO_LEVEL_F pfGetKeyIoLevel;
	uint8 u8ValidLevel;
	uint8 u8KeyCode;
	uint8 u8KeyCnt;
	uint8 u8KeyContinuousCnt;
	BOOL bKeyLongFlag;
}GLH_KEY_S;

static uint32 s_u32KeyTick = 0;
static BOOL s_bNeedKeyup = FALSE;
static uint8 s_u8DoubleKeyDownTick = 0; //双击按键检测
static GLH_KEY_S s_sKey1 = {0}, s_sKey2 = {0}, s_sKey3 = {0}, s_sKey4 = {0}, s_sKey5 = {0};

static uint8 iGetKey1IoLevel(void)
{
    if(P15 == 0)
	{
	    return 0;
	}
	else
	{
	    return 1;
	}
}

static uint8 iGetKey2IoLevel(void)
{
    if(P14 == 0)
	{
	    return 0;
	}
	else
	{
	    return 1;
	}
}

static uint8 iGetKey3IoLevel(void)
{
    if(P13 == 0)
	{
	    return 0;
	}
	else
	{
	    return 1;
	}
}

static uint8 iGetKey4IoLevel(void)
{
    if(P12 == 0)
	{
	    return 0;
	}
	else
	{
	    return 1;
	}
}

static uint8 iGetKey5IoLevel(void)
{
    if(P11 == 0)
	{
	    return 0;
	}
	else
	{
	    return 1;
	}
}

void GLHKEY_Init(void)
{
    P11_QUASI_MODE;
    P11 = 1;
    P12_QUASI_MODE;
    P11 = 1;
    P13_QUASI_MODE;
    P11 = 1;
    P14_QUASI_MODE;
    P11 = 1;
    P15_QUASI_MODE;
    P11 = 1;
	
    s_sKey1.pfGetKeyIoLevel = iGetKey1IoLevel;
	s_sKey1.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey1.u8KeyCode = 1;
	s_sKey1.u8KeyCnt = 0;
	s_sKey1.u8KeyContinuousCnt = 0;
    s_sKey1.bKeyLongFlag = FALSE;
	
    s_sKey2.pfGetKeyIoLevel = iGetKey2IoLevel;
	s_sKey2.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey2.u8KeyCode = 2;
	s_sKey2.u8KeyCnt = 0;
	s_sKey2.u8KeyContinuousCnt = 0;
    s_sKey2.bKeyLongFlag = FALSE;
	
    s_sKey3.pfGetKeyIoLevel = iGetKey3IoLevel;
	s_sKey3.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey3.u8KeyCode = 3;
	s_sKey3.u8KeyCnt = 0;
	s_sKey3.u8KeyContinuousCnt = 0;
    s_sKey3.bKeyLongFlag = FALSE;
    
    s_sKey4.pfGetKeyIoLevel = iGetKey4IoLevel;
	s_sKey4.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey4.u8KeyCode = 4;
	s_sKey4.u8KeyCnt = 0;
	s_sKey4.u8KeyContinuousCnt = 0;
    s_sKey4.bKeyLongFlag = FALSE;
    
    s_sKey5.pfGetKeyIoLevel = iGetKey5IoLevel;
	s_sKey5.u8ValidLevel = GLHKEY_VALID_LEVEL;
	s_sKey5.u8KeyCode = 5;
	s_sKey5.u8KeyCnt = 0;
	s_sKey5.u8KeyContinuousCnt = 0;
    s_sKey5.bKeyLongFlag = FALSE;
}

void GLHKEY_DeInit(void)
{

}

//按键检测
static void iKeyCheck(GLH_KEY_S *sKey)
{
	if(sKey->pfGetKeyIoLevel() == sKey->u8ValidLevel)
	{
		sKey->u8KeyCnt++;
		if(sKey->u8KeyCnt > 250)
		{
			sKey->u8KeyCnt = 250;   //防止计满溢出
		}
			
		if(sKey->u8KeyCnt == GLHKEY_SHORT_KEY_TICK)
		{
			GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_SHORT_KEYDOWN);
		}
		else if(sKey->u8KeyCnt == GLHKEY_LONG_KEY_TICK)
		{
			GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_LONG_KEYDOWN);
			sKey->bKeyLongFlag = TRUE;
            sKey->u8KeyContinuousCnt = 0;				
		}
			
		if(sKey->bKeyLongFlag == TRUE)
		{
			sKey->u8KeyContinuousCnt++;
			if(sKey->u8KeyContinuousCnt == GLHKEY_CONTINUOUS_TICK)
			{
				sKey->u8KeyContinuousCnt = 0;
			    GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_CONTINUOUS);
			}
		}
	}
	else
	{
		if((sKey->u8KeyCnt >= GLHKEY_SHORT_KEY_TICK) && (sKey->u8KeyCnt < GLHKEY_LONG_KEY_TICK))
		{
			GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_SHORT_KEYUP);			
		}
		else if(sKey->u8KeyCnt >= GLHKEY_LONG_KEY_TICK)
		{
			GLHKEY_EventCb(sKey->u8KeyCode, E_KEY_ACTION_LONG_KEYUP);			
		}	
		sKey->u8KeyCnt = 0;
		sKey->bKeyLongFlag = FALSE;
        sKey->u8KeyContinuousCnt = 0;
	}
}

void GLHKEY_MainThread(void)
{
    static uint32 s_u32LastKeyCheckTime = 0;

	if(GulSystickCount - s_u32LastKeyCheckTime > GLHKEY_THREAD_INTERVAL)
	{
	    s_u32LastKeyCheckTime = GulSystickCount;
		
		iKeyCheck(&s_sKey1);
		iKeyCheck(&s_sKey2);
		iKeyCheck(&s_sKey3);
		iKeyCheck(&s_sKey4);
		iKeyCheck(&s_sKey5);
	}
}

#endif
