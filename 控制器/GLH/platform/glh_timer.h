/*
* Copyright (c) 2020, 深圳市君同科技有限公司
* All rights reserved.
*
* 文件名称：jt_timer.h
* 文件标识：
* 摘 要： 
*     定时模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2020年10月9日
*/
#ifndef __JT_TIMER_H__
#define __JT_TIMER_H__
#include "jt_typedef.h"

//回调函数
typedef void (* JTTMR_Callback_F)(void);

/**
   @功能: 初始化对应编号的定时器
   @参数:
        u8TimerNum[in]: 定时器编号0~3
        u32Us[in]: 定时的us数，不小于4
        pfnCallback[in]: 对应定时器的回调
    @返回: 
*/
void JTTMR_Init(uint8 u8TimerNum, uint32 u32Us, JTTMR_Callback_F pfnCallback);

/**
   @功能: 反初始化对应编号的定时器，调用该接口后，对应的定时器会停止工作
   @参数:
        u8TimerNum[in]: 定时器编号0~3
    @返回: 
*/
void JTTMR_DeInit(uint8 u8TimerNum);

#endif // __JT_TIMER_H__