/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_adc.h
* 文件标识：
* 摘 要：
*    adc模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年7月16日
*/
#ifndef __GLH_ADC_H__
#define __GLH_ADC_H__
#include "glh_typedef.h"

// ADC 通道定义
#define GLHADC_CHANNEL_0   (1<<0)
//#define GLHADC_CHANNEL_1   (1<<1)
//#define GLHADC_CHANNEL_2   (1<<2)
//#define GLHADC_CHANNEL_3   (1<<3)
//#define GLHADC_CHANNEL_4   (1<<4)
//#define GLHADC_CHANNEL_5   (1<<5)
//#define GLHADC_CHANNEL_6   (1<<6)
//#define GLHADC_CHANNEL_7   (1<<7)
//#define GLHADC_CHANNEL_8   (1<<8)
//#define GLHADC_CHANNEL_9   (1<<9) 
//#define GLHADC_CHANNEL_1V4_VDD   (1<<15)  

/**
    @功能: 初始化ADC
    @参数:
           u8AdcCh[in]: 需要初始化的ADC通道集合，取值参考ADC 通道定义
    @返回:
*/
void GLHADC_Init(uint16 u16AdcCH);

/**
    @功能: 获取对应通道的ADC值
    @参数: 
        u16AdcCH[in]: 通道宏定义，一次只能输入一个通道的宏定义
    @返回: ADC值，12位数据
*/
uint16 GLHADC_Get(uint16 u16AdcCH);

BOOL bIsAllowToGetAdc(void);
void ClearAllowToGetAdc(void);
#endif //__GLH_ADC_H__