/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_pwm.h
* 文件标识：
* 摘 要：
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022年6月15日
*/
#include "MS51_16K.H"
#include "glh_pwm.h"

#ifdef SUPPORT_PWM

#define PWM_COUNT_VALUE 8000

void GLHPWM_Init(uint8 u8Ch)  //PWM1_P11
{  
    if(u8Ch & E_PWM_CH0)
    {
        P12 = 0;
        P12_PUSHPULL_MODE;        
        PWM0H = 0;
        PWM0L = 0;
    }
    
    if(u8Ch & E_PWM_CH1)
    {
        P11 = 0;
        P11_PUSHPULL_MODE;
        PWM1H = 0;
        PWM1L = 0;
    }
    
    if(u8Ch & E_PWM_CH2)
    {
        P10 = 0;
        P10_PUSHPULL_MODE;
        PWM2H = 0;
        PWM2L = 0;
    }
    
    if(u8Ch & E_PWM_CH3)
    {
        P04 = 0;
        P04_PUSHPULL_MODE;
        PWM3H = 0;
        PWM3L = 0;
    }
    
//    if(u8Ch & E_PWM_CH4)
//    {
//        P01 = 0;
//        P01_PUSHPULL_MODE;
//        set_SFRS_SFRPAGE;            //PWM4 and PWM5 duty seting is in SFP page 1
//        PWM4H = 0;
//        PWM4L = 0; 
//        clr_SFRS_SFRPAGE;
//    }
    
    if(u8Ch & E_PWM_CH5)
    {
        P03 = 0;
        P03_PUSHPULL_MODE;
        set_SFRS_SFRPAGE;            //PWM4 and PWM5 duty seting is in SFP page 1
        PWM5H = 0;
        PWM5L = 0;
        clr_SFRS_SFRPAGE;
    }
    
    PWM_IMDEPENDENT_MODE;
    PWM_CLOCK_DIV_1;
    PWMPH = (PWM_COUNT_VALUE - 1) >> 8;
    PWMPL = PWM_COUNT_VALUE - 1;
/**********************************************************************
  PWM frequency = Fpwm/((PWMPH,PWMPL) + 1) <Fpwm = Fsys/PWM_CLOCK_DIV> 
                = (16MHz/8)/(0x7CF + 1)
                = 1KHz (1ms)
***********************************************************************/
    
    PWM_OUTPUT_ALL_NORMAL;
    
/*-------- PWM start run--------------*/ 
    set_PWMCON0_LOAD;
    set_PWMCON0_PWMRUN;
    
    if(u8Ch & E_PWM_CH0)
    {
        PWM0_P12_OUTPUT_ENABLE;
    }
    
    if(u8Ch & E_PWM_CH1)
    {
        PWM1_P11_OUTPUT_ENABLE;
    }
    
    if(u8Ch & E_PWM_CH2)
    {
        PWM2_P10_OUTPUT_ENABLE;
    }
    
    if(u8Ch & E_PWM_CH3)
    {
        PWM3_P04_OUTPUT_ENABLE;
    }
    
//    if(u8Ch & E_PWM_CH4)
//    {
//        PWM4_P01_OUTPUT_ENABLE;
//    }
    
    if(u8Ch & E_PWM_CH5)
    {
        PWM5_P03_OUTPUT_ENABLE;
    }
}


void GLHPWM_Set(GLHPWM_CHANNEL_E eCh, uint32 u32Duty)
{
    if(eCh & E_PWM_CH0)
    {
        PWM0H = (unsigned char)(((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE)) >> 8);
        PWM0L = (unsigned char)((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE));	
    }
    
    if(eCh & E_PWM_CH1)
    {
        PWM1H = (unsigned char)(((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE)) >> 8);
        PWM1L = (unsigned char)((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE));	
    }
    
    if(eCh & E_PWM_CH2)
    {
        PWM2H = (unsigned char)(((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE)) >> 8);
        PWM2L = (unsigned char)((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE));	  
    }
    
    if(eCh & E_PWM_CH3)
    {
        PWM3H = (unsigned char)(((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE)) >> 8);
        PWM3L = (unsigned char)((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE));	  
    }
    
    if(eCh & E_PWM_CH4)
    {
        set_SFRS_SFRPAGE;            //PWM4 and PWM5 duty seting is in SFP page 1
        PWM4H = (unsigned char)(((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE)) >> 8);
        PWM4L = (unsigned char)((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE));	
        clr_SFRS_SFRPAGE;
    }
    
    if(eCh & E_PWM_CH5)
    {
        set_SFRS_SFRPAGE;            //PWM4 and PWM5 duty seting is in SFP page 1
        PWM5H = (unsigned char)(((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE)) >> 8);
        PWM5L = (unsigned char)((uint32)((float)u32Duty * 1.0 /GLHPWM_MAX*PWM_COUNT_VALUE));	
        clr_SFRS_SFRPAGE;
    }
    
    set_PWMCON0_LOAD;
}
#endif