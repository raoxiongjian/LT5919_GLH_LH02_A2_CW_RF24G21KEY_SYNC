/*
* Copyright (c) 2020, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_watchdog.h
* 文件标识：
* 摘 要：
*   看门狗
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年6月18日
*/
#ifndef __GLH_WATCHDOG_H__
#define __GLH_WATCHDOG_H__
#include "glh_typedef.h"

// TODO: 可通过SUPPORT_WD控制是否实现看门狗功能

/**
    @功能: 看门狗初始化
    @参数: 
    @返回: 
*/
void GLHWD_Init(void);

/**
    @功能: 
    @参数: 
    @返回: 
*/
void GLHWD_Start(void);

/**
    @功能: 
    @参数: 
    @返回: 
*/
void GLHWD_Stop(void);

/**
    @功能: 
    @参数: 
    @返回: 
*/
void GLHWD_Clear(void);

#endif //__GLH_WATCHDOG_H__
