/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_pwm.h
* 文件标识：
* 摘 要： 
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2022年8月3日
*/
#include "glh_typedef.h"

#ifdef SUPPORT_OVER_CURRENT_PROTECT

#ifndef __GLH_OVER_CUREENT_PROTECT_H__
#define __GLH_OVER_CUREENT_PROTECT_H__

typedef enum _GLH_OCP_EVENT_E
{
    E_GLH_OCP_EVENT_PROTEC = 0,
    E_GLH_OCP_EVENT_UNPROTEC,
}GLH_OCP_EVENT_E;

extern void GLHOCP_EventCallBack(GLH_OCP_EVENT_E eEvent);


/*
    功能: 过流保护模块初始化
*/
void GLHOCP_Init(void);

/*
    功能: 过流保护主线程
*/
void GLHOCP_MainThread(void);


#endif //ifndef __GLH_OVER_CUREENT_PROTECT_H__

#endif //ifdef SUPPORT_OVER_CURRENT_PROTECT

