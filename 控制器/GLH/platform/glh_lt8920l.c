#include "MS51_16K.H"
#include "glh_lt8920l.h"
#include "glh_delay.h"
#include "glh_sys_tick.h"

#define  RF_RST_1       P05=1
#define  RF_RST_0       P05=0

#define  SPI_CS_1	    P17=1 
#define  SPI_CS_0       P17=0

#define  SPI_CLK_1	    P30=1
#define  SPI_CLK_0	    P30=0

#define  SPI_MOSI_1     P07=1
#define  SPI_MOSI_0     P07=0

#define  SPI_MISO       P06

static uint8 s_u8ResH = 0, s_u8ResL = 0;

static void iSpiWriteByte(uint8 u8Byte)
{
    uint8 u8Cnt = 0;
    
	for(u8Cnt=0; u8Cnt<8; u8Cnt++)
	{
		SPI_CLK_1;
		_nop_();
        _nop_();
		if(u8Byte & 0x80)
        {
            SPI_MOSI_1;
        }
		else
        {
            SPI_MOSI_0;
        }
		_nop_();
        _nop_();
		u8Byte <<= 1;
		SPI_CLK_0;
	}
    
	SPI_MOSI_0;
}

static uint8 iSpiReadByte(void)
{
	uint8 u8Cnt = 0, u8Ret = 0;
    
	for(u8Cnt=0; u8Cnt<8; u8Cnt++)
	{
		SPI_CLK_1;
		_nop_();
        _nop_();
		u8Ret <<= 1;
		if(SPI_MISO)
		{
			u8Ret |= 0x01;
		}
		_nop_();
        _nop_();
		SPI_CLK_0;
	}
    
	return u8Ret;
}

void RF24G_WriteReg(uint8 u8Reg, uint16 u16Value)
{
	SPI_CS_0;
	iSpiWriteByte(u8Reg);
	iSpiWriteByte(u16Value >> 8);
	iSpiWriteByte(u16Value);
	SPI_CS_1;    
}

uint16 RF24G_ReadReg(uint8 u8Reg)
{
    uint16 u16Temp = 0;

	SPI_CS_0;
	iSpiWriteByte(u8Reg + 0x80);
	u16Temp |= (iSpiReadByte() << 8);
	u16Temp |= iSpiReadByte();
	SPI_CS_1;	
    
    return u16Temp;
}


void RF24G_Change_0x38(void)
{
    static BOOL s_bFlag = FALSE;
    
    s_bFlag = !s_bFlag;
	if(s_bFlag)
	{
		RF24G_WriteReg(0x38, 0xBCDF);
	}
	else
	{
		RF24G_WriteReg(0x38, 0xBFFF);
	}
}

void RF24G_Init(void)
{
    uint32 u32WaitStartTime = 0;

    //设置SPI引脚模式    
    P05_PUSHPULL_MODE;
    P07_PUSHPULL_MODE;
    P17_PUSHPULL_MODE;
    P30_PUSHPULL_MODE;
    
    P06_INPUT_MODE;
    
    ReTry:
    //唤醒系统
    /*
    @bit[1]: I2c_soft_rst  I2C软件复位 Writing ‘0’ to soft reset system. 
    @bit[0]: wakeup_i2c    I2C唤醒 Writing ‘0’ to wake up system.
    */
    RF_RST_0;
    //等待10ms
    u32WaitStartTime = GulSystickCount;
    while(GulSystickCount - u32WaitStartTime < 20);  
    //复位系统
    RF_RST_1;
    //等待10ms
    u32WaitStartTime = GulSystickCount;
    while(GulSystickCount - u32WaitStartTime < 20);  

    //检测能否正常读取 RF24G 内部寄存器数据    
	if(RF24G_ReadReg(0) != 0x6fe0)	              
	{
		goto ReTry; 
	}	
    
    //设备工作状态设置，拼点配置
    /*
    @bit[8]:  使芯片进入TX状态，1有效  
    @bit[7]:  使芯片进入RX状态，1 有效
    @bit[6:0]: 设定RF频道，空中频率为：f=2402+PLL_CH_NO 
    */
    RF24G_WriteReg(7, 0x004c);   
    
    //没有相关寄存器介绍，根据例程写入一个值    
    RF24G_WriteReg(8, 0x6c90);
    
    
    //PA功率配置 
    /*
    @bit[15:12]: PA_PWCTR [3:0]  PA 电流控制. 
    @bit[11:7]:  PA_GN_reg[4:0]  PA 增益 1
    @bit[6:4]:   PAGV[2:0]       PA 增益 2
    */
	RF24G_WriteReg(9, 0x7830);
    
    
    //设置频偏  
    /*
    @bit[13:0]: Ref_fq[13:0]  当前频点负偏  value*405k. 
    */        
    RF24G_WriteReg(28, 0x1800);
    
    
    //射频配置 :前导码，同步字，trailer_len,编码方式
    /*
    @bit[15:13]: preamble_len  000: 1byte, 001: 2bytes, 010: 3 bytes, ... 111: 8 bytes 
    @bit[12:11]: syncword_len  11: 64bits(另外两字节为固定 REG036 37 38 39) 
                               10: 48bits（REG36 38 39） 
                               01: 32bits,{ Reg39[15:0],Reg36[15:0]} 
                               00: 16 bits,{ Reg39[15:0]}
    @bit[10:8]: trailer_len    000: 4 bits, 001: 6bits, 010: 8 bits, 011: 10 bits ... 111: 18bits 
    @bit[7:6]:  pack_type      00: NRZ law data。  01: Manchester data type。 10: 8/10 line code。 11: interleave date type。 
    */        
    RF24G_WriteReg(32, 0x4800);
    
    
    //SLEEP_MODE 配置，bit14写1进入sleep
    /*
    @bit[14]: SLEEP_MODE  写 1进入  sleep mode，先关闭晶体振荡器，再关闭  LDO（寄存器值将丢失）当IIC 唤醒时，芯片将重新工作 
    */
    RF24G_WriteReg(35, 0x0300);
    
    
    //同步字配置    
    RF24G_WriteReg(36, 0x9aab);  
    RF24G_WriteReg(39, 0xbccd);
    
    
    //FIFO溢出控制，同步字容错设置 
    /*
    @bit[15:11]: empty_thres    FIFO空 设定
    @bit[10:6]:  full_thres     FIFO满 设定
    @bit[5:0]:   sync_thres     认为 SYNCWORD为正确的阈值 07H表示可以错6bits，01H表示可以错0bits 
    */     
    RF24G_WriteReg(40, 0x4401); 
    
    
    //收发模式设置:
    /*
    @bit[15]:crc_on , 1:开启CRC16  0:关闭CRC16
    @bit[13]:pack_length_en, 1: 第一字节表示  payload  的长度,接收机把收到的第一字节
                                作为长度信息,待收到目标长度的数据后,停止接收机。如要写 
                                8个byte有效字节，那第一个字节应写8，总长9。
                             0: 由MCU控制接收机停止接收。
    @bit[12]:fw_hw_term_en,  1: 当  FIFO 的读指针和写指针相等时，将关闭发射。
                             0: 由  MCU 确定长度并关闭发射。
    @bit[11]:AUTO_ACK        1: 当接收到数据，自动回  ACK 或者  NACK
                             0: 接收数据后，不回ACK，直接进IDLE 
    @bit[10]:PKT_POLARITY,   1: PKT flag低有效，收到数据 SDA脚被拉低。
                             0: 高有效收到数据时 SDA 脚被拉高。
    @bit[7:0]: CRC_INITIAL_DATA CRC 计算初始值
    */    
    RF24G_WriteReg(41, 0xB000);
    

    //AUTO_ACK 时间设置 
    /*
    @bit[9:8]: wake_up_tim[1:0]         vlaue*4us
    @bit[7:0]: auto_rx_ack_time[7:0]    等待 RX_ACK 的时间 ，1 表示1us(1Mbps) ,value*16us(62.5Kbps), 再加上 64us 
    */
    RF24G_WriteReg(42, 0xFDB0);


    //通讯速率配置
    /*
    @bit[15:8]:DATARATE[7:0] ,  01H: 1Mbps 
                                04H: 250Kbps 
                                08H: 125Kbps 
                                10H: 62.5Kbps 
    */        
    RF24G_WriteReg(44, 0x0400);//250kbps
    /*
    @bit[15:0]: Modem option  通讯速率是1Mbps的时候配置是0080H,  通讯速率是其他的时候配置最好是 0552H 
    */
    RF24G_WriteReg(45, 0x0080); 
    
    
    //FIFO读写指针位置
    /*
    @bit[15:0]: fw_clr_wr_ptr  1: 清空  TX FIFO 指针为  0
    @bit[13:8]: fifo_wr_ptr[5:0]  FIFO 写指针
    @bit[7]: fw_clr_rd_ptr     1: 清空  RX FIFO 指针为  0
    */
    RF24G_WriteReg(52, 0x8080);	
}
void RF24G_Tx(const uint8 *pu8Data, uint8 u8Len, uint8 u8Ch)
{
    uint8 i = 0;
    
    if(pu8Data == NULL || u8Len > 63)
    {
        return;
    }
    
    //进入空闲模式，设置通道为0x30
    RF24G_WriteReg(7, 0x0000);
    //清空TX FIFO指针
	RF24G_WriteReg(52, 0x8080);           

    //向fifo写入需要发送的数据
    SPI_CS_0;
	iSpiWriteByte(50);
	iSpiWriteByte(u8Len);	
	for(i=0; i<u8Len; i++)
	{
		iSpiWriteByte(pu8Data[i]);
	}
    SPI_CS_1;
						
	RF24G_WriteReg(8, 0x6C90);
    //使芯片进入TX状态
	RF24G_WriteReg(7, 0x0100 + u8Ch);  
}

void RF24G_EnterRxMode(uint8 u8Ch)
{
    //进入空闲模式，设置通道为0x30
    RF24G_WriteReg(7, 0x0030);
    //这里起什么作用不是很清楚，按照例程添加这一条指令
	RF24G_Change_0x38();
	RF24G_WriteReg(8, 0x6c90);
	RF24G_WriteReg(52, 0x8080);
    //使芯片进入RX状态
	RF24G_WriteReg(7, 0x0080 + u8Ch);
}

void RF24G_ReadRxData(uint8 *pu8RxData, uint8 *pu8RxLen)
{
    uint8 i = 0;

    SPI_CS_0;    
	iSpiWriteByte(50|0x80);        
    *pu8RxLen = iSpiReadByte();
	if(*pu8RxLen > 63)
	*pu8RxLen = 63;
	for(i=0; i<*pu8RxLen; i++)
	{
		pu8RxData[i] = iSpiReadByte(); 
	}
    SPI_CS_1;
}

RF24G_REC_RESULT_E RF24G_IsTxOrRxComplete(void)
{
    uint16 u16Temp = 0;
    
	u16Temp = RF24G_ReadReg(48);

	if(u16Temp & 0x0040)
    {
        if(u16Temp & 0x8000)
        {
            return E_RF24G_REC_STATUS_ERROR;
        }
        else
        {
            return E_RF24G_REC_STATUS_OK;
        }
    }
	else
    {
        return E_RF24G_REC_STATUS_NO;
    }
}
