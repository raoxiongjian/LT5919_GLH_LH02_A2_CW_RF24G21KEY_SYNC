/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_adc.c
* 文件标识：
* 摘 要：
*    adc模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期: 2021年9月23日
*/
#include "MS51_16K.H"
#include "glh_adc.h"

//00 = ADC時鐘分頻為 FADC = FSYS/1.
//01 = ADC時鐘分頻為 FADC = FSYS/2.
//10 = ADC時鐘分頻為 FADC = FSYS/4.
//11 = ADC時鐘分頻為 FADC= FSYS/8

/* if define TIMER0_FSYS_DIV12, timer = (256-56)*12/246MHz = 100us */
/* if define TIMER0_FSYS, timer = (256-56)/24MHz = 8.33us */


void GLHADC_Init(uint16 u16AdcCH)
{
    if(u16AdcCH & GLHADC_CHANNEL_0)
    {
        ENABLE_ADC_AIN0;
    }
    
    ADCCON1 |= 0x30;            /* clock divider */
    ADCCON2 |= 0x01;            /* AQT time */
    AUXR1 &= CLR_BIT4;          /* ADC clock high speed */
}

uint16 GLHADC_Get(uint16 u16AdcCH)
{
    uint16 u16AdcVal = 0;
    
    clr_ADCCON0_ADCF;
    set_ADCCON0_ADCS;                  // ADC start trig signal
    while(ADCF == 0);
    u16AdcVal = (ADCRH<<4) + ADCRL;
    
    return u16AdcVal;
}
