/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_pwm.h
* 文件标识：
* 摘 要：
*     通过定时器产生pwm
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022年8月3日
*/
#include "MS51_16K.H"
#include "glh_over_current_protect.h"
#include "glh_sys_tick.h"

#ifdef SUPPORT_OVER_CURRENT_PROTECT

#define OVER_CURRENT_PROTECT_TIME     2000
#define PROTECT_TRIG_IO_LEVEL         1

static BOOL s_bIsOverCurrent = FALSE;
static uint32 s_u32OverCurrentStartTime = 0;

void GLHOCP_Init(void)
{
    #if 0
    ENABLE_INT_PORT1;
    ENABLE_BIT3_FALLINGEDGE_TRIG;
    ENABLE_PIN_INTERRUPT;
    #endif
    
	PICON = 0X21;//P1.3
	PINEN = 0X08;
	PIPEN = 0X00;
	set_EPI;
}

void GLHOCP_MainThread(void)
{
    if(s_bIsOverCurrent == TRUE)       //处于保护状态
    {
        if(GulSystickCount - s_u32OverCurrentStartTime > OVER_CURRENT_PROTECT_TIME)    //超过解除保护的时间
        {
            s_bIsOverCurrent = FALSE;
            PMEN = 0;
            GLHOCP_EventCallBack(E_GLH_OCP_EVENT_UNPROTEC);                  //解除保护
        }
    }
}

void Port_IRQHandler(void) interrupt 7
{
    if(PIF & 0x08)
    {
		PIF = 0;
        
        PMEN = 0xff;    //禁止所有PWM通道输出
		PMD = 0X00;
        GLHOCP_EventCallBack(E_GLH_OCP_EVENT_PROTEC);     //执行保护操作
        s_bIsOverCurrent = TRUE;
        s_u32OverCurrentStartTime = GulSystickCount;    //记录保护开始的时间
    }
}

#endif