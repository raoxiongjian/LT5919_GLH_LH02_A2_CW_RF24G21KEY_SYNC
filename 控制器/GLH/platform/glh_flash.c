/*
* Copyright (c) 2022, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_flash.c
* 文件标识：
* 摘 要：
*    读写flash模块
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2022年5月30日
*/
#include "glh_flash.h"
#include "eeprom.h"

#ifdef SUPPORT_FLASH

#define UUID_ADDR   0x3ffc

void GLHFLASH_Init(void)
{

}

void GLHFLASH_EraseBlock(uint32 u32Addr, uint16 u16Len)
{
    u32Addr = u16Len;               //无实际意义，仅仅只是为了去掉编译时产生的警告
}

uint8 GLHFLASH_ReadByte(uint32 u32Addr)
{
    uint8 u8Temp = 0;
    
    u8Temp = read_APROM_BYTE((unsigned int CODE *)u32Addr);
    
    return u8Temp;
}

void GLHLASH_WriteByte(uint32 u32Addr, uint8 u8Byte)
{
    Write_DATAFLASH_BYTE(u32Addr, u8Byte);
}

void GLHFLASH_ReadBytes(uint32 u32Addr, uint8 *pu8Buff, uint16 u16Len)
{
    if (pu8Buff == NULL || u16Len == 0 || u32Addr < GLHFLASH_START_ADDR || (u32Addr+u16Len-1) > GLHFLASH_END_ADDR)
    {
        return;
    }
    
    Read_DATAFLASH_ARRAY(u32Addr, pu8Buff, u16Len);    
}


void GLHFLASH_WriteBytes(uint32 u32Addr, uint8 *pu8Buff, uint16 u16Len)
{
    if (pu8Buff == NULL || u16Len == 0 || u32Addr < GLHFLASH_START_ADDR || (u32Addr+u16Len-1) > GLHFLASH_END_ADDR)
    {
        return;
    }
    
    Write_DATAFLASH_ARRAY(u32Addr, pu8Buff,u16Len);
}

void GLHFLASH_ReadDevId(uint8 *pu8Buff)
{
    Read_DATAFLASH_ARRAY(UUID_ADDR, pu8Buff, 4);
}

#endif
