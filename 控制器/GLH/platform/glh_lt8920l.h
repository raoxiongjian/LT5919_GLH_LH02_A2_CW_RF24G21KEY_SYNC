#ifndef __RF_DRIVER_H__
#define __RF_DRIVER_H__
#include "glh_typedef.h"
typedef enum _RF24G_REC_RESULT_E
{
    E_RF24G_REC_STATUS_UNKNOWN,
    E_RF24G_REC_STATUS_OK,
    E_RF24G_REC_STATUS_NO,
    E_RF24G_REC_STATUS_ERROR,
}RF24G_REC_RESULT_E;

void RF24G_Init(void);
void RF24G_ReadRxData(uint8 *pu8RxData, uint8 *pu8RxLen);
RF24G_REC_RESULT_E RF24G_IsTxOrRxComplete(void);
void RF24G_Tx(const uint8 *pu8Data, uint8 u8Len, uint8 u8Ch);
void RF24G_EnterRxMode(uint8 u8Ch);

#endif

