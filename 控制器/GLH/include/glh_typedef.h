/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_typedef.h
* 文件标识：
* 摘 要：
*   该文件主要用来定义变量的类型和实现一些基本的方法，实现不同平台的类型统一
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月16日
*/
#ifndef __GLH_TYPEDEF_H__
#define __GLH_TYPEDEF_H__
//平台头文件 文件
#include <string.h>
//#include <intrins.h>

#ifndef __INT_SIZE__
#define __INT_SIZE__    2
#endif

#ifdef  __cplusplus
extern "C" {
#endif

typedef unsigned char         BOOL;
typedef unsigned char         BOOL_t;
typedef unsigned char         uint8;
typedef signed char           int8;

#if (__INT_SIZE__ == 2)
typedef unsigned int    uint16;
typedef unsigned long   uint32;
typedef signed int  int16;
typedef signed long int32;
#elif (__INT_SIZE__ == 4) /////
typedef unsigned short    uint16;
typedef unsigned int   uint32;
typedef signed short  int16;
typedef signed int int32;
#else
#error "No Support Platform!"
#endif

#include "app_user_config.h"

#ifndef FALSE
#define FALSE   0
#endif

#ifndef TRUE
#define TRUE    (!FALSE)
#endif

#ifndef NULL
#define NULL    0
#endif

#ifndef ON
#define ON      1           // 有些平台io的on状态为一个非零值
#endif

#ifndef OFF
#define OFF     0
#endif

#ifdef  __cplusplus
}
#endif

#endif //__GLH_TYPEDEF_H__
