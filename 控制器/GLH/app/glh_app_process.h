/*
* Copyright (c) 2021, 深圳市光丽海科技有限公司
* All rights reserved.
*
* 文件名称：glh_app_process.h
* 文件标识：
* 摘 要： 
*	主要对私有化的任务做处理
* 当前版本：V1.0
* 作 者： raoxiongjian
* 完成日期：2021年6月18日
*/
#ifndef __JT_APP_PROCESS_H__
#define __JT_APP_PROCESS_H__

#include "glh_typedef.h"

void GLHAPS_Init(void);

void GLHAPS_MainThread(void);

#endif //__JT_APP_PROCESS_H__
